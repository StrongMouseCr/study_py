import pygame as pg
from random import randint
import numpy as np

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")

radius = 10
speed = 10
user = [[40, 40]]

soundMoney = pg.mixer.Sound("C:\\MyDoc\\Projects\\logSim\\money.mp3")

constNumFood = 100
numFood = constNumFood
minDist = 100000
numMin = 0
food = []
for i in range(0, numFood):
    food.append([np.random.randint(0 + radius, display.get_width() - radius), np.random.randint(0 + radius, display.get_height() - radius)])
for i in range(0, numFood):
    if np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1])) < minDist:
        numMin = i
        minDist = np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1]))
while True:
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()

    display.fill((100,0,255))

    pg.draw.circle(display, (255, 0, 0), (user[0][0], user[0][1]), radius) 
    for i in range(0, numFood):
        pg.draw.circle(display, (0, 255, 0), (food[i][0], food[i][1]), radius) 
    '''if key[pg.K_w]:
        user[0][1] -= 1 * speed
    if key[pg.K_a]:
        user[0][0] -= 1 * speed
    if key[pg.K_s]:
        user[0][1] += 1 * speed
    if key[pg.K_d]:
        user[0][0] += 1 * speed'''

    if user[0][0] - radius < 0:
        user[0][0] += 10
    if user[0][0] + radius > display.get_width():
        user[0][0] -= 10
    if user[0][1] - radius < 0:
        user[0][1] += 10
    if user[0][1] + radius > display.get_height():
        user[0][1] -= 10

    if food[numMin][0] - user[0][0] < 0:
        user[0][0] -= 1 * speed
    if food[numMin][0] - user[0][0] > 0:
        user[0][0] += 1 * speed
    if food[numMin][1] - user[0][1] > 0:
        user[0][1] += 1 * speed
    if food[numMin][1] - user[0][1] < 0:
        user[0][1] -= 1 * speed

    if np.sqrt(np.abs(food[numMin][0] - user[0][0]) * np.abs(food[numMin][0] - user[0][0]) + np.abs(food[numMin][1] - user[0][1]) * np.abs(food[numMin][1] - user[0][1])) <= 2 * radius:
        food.pop(numMin)
        soundMoney.play()
        numFood -= 1
        if numFood == 0:
            numFood = constNumFood
            for i in range(0, numFood):
                food.append([np.random.randint(0 + radius, display.get_width() - radius), np.random.randint(0 + radius, display.get_height() - radius)])
            minDist = 100000
            for i in range(0, numFood):
                if np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1])) < minDist:
                    numMin = i
                    minDist = np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1]))
        else:
            minDist = 100000
            for i in range(0, numFood):
                if np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1])) < minDist:
                    numMin = i
                    minDist = np.sqrt(np.abs(food[i][0] - user[0][0]) * np.abs(food[i][0] - user[0][0]) + np.abs(food[i][1] - user[0][1]) * np.abs(food[i][1] - user[0][1]))

    pg.display.update()
    clock.tick(60)