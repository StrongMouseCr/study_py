import pygame as pg
import numpy as np

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")

fontSize = 25
font = pg.font.SysFont('ms mincho', fontSize)

speed = 1

lenList = 10
column = 1000
step = display.get_width() / column

numNum = 400

numList = []
def generateLine():
    lineList = [[np.random.randint(0, 10), np.random.randint(0, column - 1), np.random.randint(0, display.get_height() - 100), np.random.randint(1, 6)]]
    for i in range(1, lenList):
        lineList.append([np.random.randint(0, 10), lineList[0][1], lineList[i - 1][2] - 20, lineList[i - 1][3]])
    return lineList

for i in range(0, numNum):
    numList.append(generateLine())

while True:
    display.fill((0,0,0))
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
    for i in range(0, numNum):
        alpha = 225
        for j in range(0, len(numList[i])):
            num = font.render(str(numList[i][j][0]), True, (0, 255, 0))
            num.set_alpha(alpha)
            alpha -= 20
            numList[i][j][2] += numList[i][j][3]
            display.blit(num, (numList[i][j][1] * step, numList[i][j][2]))
            if numList[i][j][2] > display.get_height():
                numList[i][j][2] = -25

    pg.display.flip()
    clock.tick(120)