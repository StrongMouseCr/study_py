import pygame as pg
from random import randint
import numpy as np
from math import atan2, degrees, pi

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")

radius = 10
user = [400, 600]
speed = 10
angle = 0

numColumn = 20
numRow = 15

stepX = display.get_width() // numColumn
stepY = display.get_height() // numRow

patternMap = [[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
              [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
]

def findY(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.sin(angle)

def findX(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.cos(angle)

def findDistance(x1, y1, x2, y2):
    dx = np.abs(x1- x2)
    dy = np.abs(y1 - y2)
    distance = np.sqrt(dx * dx + dy * dy)
    return distance

def findAngle(x2, y2, x1, y1):
    dx = np.abs(x2 - x1)
    dy = np.abs(y2 - y1)
    rads = atan2(-dy,dx)
    rads %= 2 * pi
    degs = 360 - degrees(rads)
    return degs

while True:
    display.fill((0, 0, 0))
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
    pg.draw.circle(display, (255, 0, 0), (user[0], user[1]), radius)

    pg.draw.line(display, (255, 255, 255), (user[0], user[1]), (user[0] + 2 * display.get_width() * np.cos(angle), user[1] + 2 * display.get_width() * np.sin(angle)))


    if key[pg.K_w]:
        user[0] += speed * np.cos(angle)
        user[1] += speed * np.sin(angle)
    if key[pg.K_a]:
        user[0] += speed * np.sin(angle)
        user[1] -= speed * np.cos(angle)
    if key[pg.K_s]:
        user[0] -= speed * np.cos(angle)
        user[1] -= speed * np.sin(angle)
    if key[pg.K_d]:
        user[0] -= speed * np.sin(angle)
        user[1] += speed * np.cos(angle)
    for i in range(0, numRow):
        for j in range(0, numColumn):
            if patternMap[i][j] == 1:
                pg.draw.rect(display, (255, 255, 255), (j * stepX, i * stepY, stepX, stepY), 4)


    if angle == 360:
        angle = 0
    elif angle == 0:
        angle = -360
    if key[pg.K_q]:
        angle -= 0.02
    elif key[pg.K_e]:
        angle += 0.02

    pg.display.update()
    clock.tick(60)