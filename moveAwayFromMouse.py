import pygame as pg
from random import randint
import numpy as np
from math import atan2, degrees, pi

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")

eventX = 0
eventy = 0

numDots = 1000
radius = 5
dots = []
speed = 100
speedForMouse = 5

def findDistance(x1, y1, x2, y2):
    dx = np.abs(x1- x2)
    dy = np.abs(y1 - y2)
    distance = np.sqrt(dx * dx + dy * dy)
    return distance

def findY(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.sin(angle)

def findX(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.cos(angle)

def findAngle(x2, y2, x1, y1):
    dx = np.abs(x2 - x1)
    dy = np.abs(y2 - y1)
    rads = atan2(-dy,dx)
    rads %= 2 * pi
    degs = 360 - degrees(rads)
    return degs

for i in range(0, numDots):
    dots.append([np.random.randint(radius, display.get_width() - radius), np.random.randint(radius, display.get_height() - radius)])
while True:
    display.fill((0, 0, 0))
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
    eventX, eventY = pg.mouse.get_pos()

    for i in range(0, numDots):
        if np.random.randint(0, 2) == 0:
            dots[i][0] += np.random.randint(-100, 100) / speed
        else:
            dots[i][1] += np.random.randint(-100, 100) / speed
        pg.draw.circle(display, (255, 0, 0), (dots[i][0], dots[i][1]), radius)
        distance = findDistance(eventX, eventY, dots[i][0], dots[i][1])
        if dots[i][0] - 2 * radius < 0:
            dots[i][0] += 10
        if dots[i][0] + 2 * radius > display.get_width():
            dots[i][0] -= 10
        if dots[i][1] - 2 * radius < 0:
            dots[i][1] += 10
        if dots[i][1] + 2 * radius > display.get_height():
            dots[i][1] -= 10
        if distance < 10 * radius:
            angle = findAngle(eventX, eventY, dots[i][0], dots[i][1])
            dx = findX(distance, angle)
            dy = findY(distance, angle)

            if eventX - dots[i][0] < 0:
                dots[i][0] += dx / speedForMouse
            else:
                dots[i][0] -= dx / speedForMouse
            if eventY - dots[i][1] < 0:
                dots[i][1] += dy / speedForMouse
            else:
                dots[i][1] -= dy / speedForMouse

    pg.display.update()
    clock.tick(60)