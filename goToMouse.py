import pygame as pg
from random import randint
import numpy as np

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")

user = [400, 400]
radius = 10
speed = 10

eventX = 0
eventY = 0

while True:
    display.fill((100,0,255))
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
        if event.type == pg.MOUSEBUTTONDOWN:
            if event.button == 1:
                pg.draw.circle(display, (255, 255, 255), event.pos, radius)
                eventX = event.pos[0]
                eventY = event.pos[1]

    if eventX - user[0] < 0:
        user[0] -= 1 * speed
    if eventX - user[0] > 0:
        user[0] += 1 * speed
    if eventY - user[1] > 0:
        user[1] += 1 * speed
    if eventY - user[1] < 0:
        user[1] -= 1 * speed

    pg.draw.circle(display, (255, 255, 255), (user[0], user[1]), radius)

    pg.draw.line(display, (255, 255, 255), (user[0], user[1]), (eventX, eventY))

    pg.display.update()
    clock.tick(60)