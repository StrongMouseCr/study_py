import pygame as pg
from random import randint
import numpy as np
from math import atan2, degrees, pi

pg.init()

clock = pg.time.Clock()
display = pg.display.set_mode((0, 0), pg.FULLSCREEN)
pg.display.set_caption("Sim")


user = [500, 500]
startPos = [[500, 500]]
radius = 2

lenLine = 10
angle = 10
constAngle = 10


def findAngle(x2, y2, x1, y1):
    dx = np.abs(x2 - x1)
    dy = np.abs(y2 - y1)
    rads = atan2(-dy,dx)
    rads %= 2 * pi
    degs = 360 - degrees(rads)
    return degs

def findLenLine(x1, y1, x2, y2):
    dx = np.abs(x1- x2)
    dy = np.abs(y1 - y2)
    lenLine = np.sqrt(dx * dx + dy * dy)
    return lenLine

def findY(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.sin(angle)

def findX(lenLine, angle):
    angle = angle * pi / 180
    return lenLine * np.cos(angle)

while True:
    display.fill((100,0,255))
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
    if int(findLenLine(user[0], user[1], startPos[len(startPos) - 1][0], startPos[len(startPos) - 1][1])) <= lenLine:
        user[0] += findX(lenLine, angle)
        user[1] -= findY(lenLine, angle)
    else:
        angle -= constAngle
        if angle == -360:
            angle = 0
        startPos.append([user[0], user[1]])
    for i in range(1, len(startPos)):
        pg.draw.line(display, (255, 0, 0), (startPos[i - 1][0], startPos[i - 1][1]), (startPos[i][0], startPos[i][1]))
    pg.draw.line(display, (0, 255, 0), (startPos[len(startPos) - 1][0], startPos[len(startPos) - 1][1]), (user[0], user[1]))
    pg.draw.circle(display, (255, 255, 255), (user[0], user[1]), radius)


    pg.display.update()
    clock.tick(60)